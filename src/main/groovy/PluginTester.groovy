import com.amazonaws.regions.RegionUtils;
import com.amazonaws.regions.Region;

this.metaClass.println = {    
    logger.addBuildLogEntry(it);    
}

for (Region r : RegionUtils.getRegions())
    println "Region:" + r;
    
