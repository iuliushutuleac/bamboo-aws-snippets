import java.io.IOException;
import java.io.File;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.GetBucketLocationRequest;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;

this.metaClass.println = {    
    logger.addBuildLogEntry(it);    
}

String bucketName     = "mytestgroovybucket";
String directoryToUpload = "packages"

BasicAWSCredentials awsCreds = new BasicAWSCredentials("${bamboo.aws_access_key}", "${bamboo.aws_access_password}");
 
AmazonS3 s3client = new AmazonS3Client(awsCreds);

s3client.setRegion(Region.getRegion(Regions.US_WEST_1));

try {
    if(!(s3client.doesBucketExist(bucketName)))
    {		
        s3client.createBucket(new CreateBucketRequest(
                bucketName));
    }
	
    String bucketLocation = s3client.getBucketLocation(new GetBucketLocationRequest(bucketName));
    println "bucket location = " + bucketLocation;
    
    TransferManager tm = new TransferManager(s3client);
    
    File pck = new File("${bamboo.build.working.directory}/" + directoryToUpload);
    println "isDirectory " + pck.isDirectory();
    if(pck.isDirectory())
        {
            MultipleFileUpload upload = tm.uploadDirectory(bucketName,        
                "${bamboo.buildNumber}", new File("${bamboo.build.working.directory}/" + directoryToUpload), true);
            upload.waitForCompletion();
            tm.shutdownNow(); 
        }
    else{
        throw(new Exception("Directory not found"))
        tm.shutdownNow(); 
    }
        

} catch (Exception e) {
    println "An error occured ";
    e.printStackTrace();
    throw(e)
} 

